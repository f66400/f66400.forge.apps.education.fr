export function creer_matrice(hauteur, largeur, remplissage = 0){
	let m = [];
	let c;
	for(let i = 0; i < hauteur; i++){
			c = [];
			for(let j = 0; j < largeur; j++){
				c.push(0);
			}
			m.push(c);
		}
	return m;
}

export function matrices_egales(mat1, mat2) {
	if(mat1.length != mat2.length){
		return false   // même nombre de lignes ?
	}
	if(mat1.length > 0 && mat1[0].length != mat2[0].length){
		return false;  // même nombre de colonnes ?
	}
	for (let i = 0; i < mat1.length; i++) {
	  for (let j = 0; j < mat1[i].length; j++) {
		  if (mat1[i][j] !== mat2[i][j]) {
			  return false;
		  }
	  }
	}
	  return true;
  }
export function creer_bloc_sudoku_aleatoire(){
	let bloc = creer_matrice(3, 3);
	let chiffres = [1, 2, 3, 4, 5, 6, 7, 8, 9];
	// mélange aléatoire des chiffres
	let j;
	for(let i=0; i<9; i++){
		j = Math.floor( Math.random()*chiffres.length );
       [chiffres[i], chiffres[j]] = [chiffres[j], chiffres[i]];
	}
	for(let i = 0; i < 3; i++){
    for(let j = 0; j < 3; j++){
			bloc[i][j] = chiffres[3*i + j]
		}
	}
	return bloc
  }

export function creer_matrice_booleenne_symetrique_aleatoire(n){
	let matrice = creer_matrice(n, n);
	for(let i = 0; i<n; i++){
		for(let j = i+1; j<n; j++){
			matrice[i][j] = Math.floor( Math.random()*2 );
			matrice[j][i] = matrice[i][j]
		}
	}
	return matrice;
}
export function tab_est_matrice(tab){  // si un tableau code une matrice : tableau rectangulaire de nombres
	// Vérifie si toutes les lignes ont la même longueur
	const largeur = tab[0].length;
	for (let ligne of tab) {
	  if (!Array.isArray(ligne) || ligne.length !== largeur) {
		alert(ligne+" "+ligne.length+" "+largeur);
		return false;
	  }

	  // Vérifie que chaque élément de la ligne est un nombre
	  for (let item of ligne) {
		if (Number.isNaN(Number.parseFloat(item))) {
		  alert("item"+item);
		  return false;
		}
	  }
	}

	return true;

}
export function texte_est_matrice(texte) {      // si un texte donné peut coder une matrice
	try {
	  // Tente de parser le texte
	  const parse = JSON.parse(texte);
  
	  // Vérifie si c'est un tableau (même si un tableau vide est accepté)
	  if (!Array.isArray(parse)) {
		console.log("tableau");
		return false;
	  }
      return tab_est_matrice(parse);
	  
	} catch (e) {
	  // Si une erreur survient lors du parsing, ce n'est pas une matrice valide
	  console.log("parse");
	  return false;
	}
  }

  export function est_symetrique(matrice){  // on suppose qu'on a bien une matrice non vide
	if( matrice.length != matrice[0].length){
		return false   // pas carrée
	}else{
		const n = matrice.length;
		for(let i=0; i< n; i++){
			for(let j=0; j < i; j++){
			    if(matrice[i][j] != matrice[j][i]){
					return false
				}
			}
		}
		return true
	}
  }

  export function est_booleenne(matrice){  // uniquement des 0 et des 1 ?
	const n = matrice.length;
	const m = matrice[0].length;
	for(let i=0; i< n; i++){
		for(let j=0; j < m; j++){
			if(matrice[i][j] != 0 && matrice[i][j] != 1){
				return false
			}
		}
	}
	return true
  }
