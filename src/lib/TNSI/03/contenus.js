import { readable } from 'svelte/store';

export const contenus = readable([
    {href: "/TNSI/03/01-feuilles", texte: "Trouvez les feuilles"},
    {href: "/TNSI/03/02-taille_hauteur", texte: "Taille et hauteur"},
    {href: "/TNSI/03/03-parcours_largeur", texte: "Parcours en largeur d'abord"},
    {href: "/TNSI/03/04-parcours_prefixe", texte: "Parcours préfixe"},
    {href: "/TNSI/03/05-parcours_infixe", texte: "Parcours infixe"},
    {href: "/TNSI/03/06-parcours_suffixe", texte: "Parcours suffixe"},
    {href: "/TNSI/03/07-parcours_aleatoire", texte:"Choix de parcours aléatoire"},
    {href: "/TNSI/03/08-parcours_chronometre", texte:"Parcours chronométrés"},
    {href: "/TNSI/03/09-inserer_abr", texte: "Insérer dans un arbre binaire de recherche"},
    {href: "/TNSI/03/10-valeurs_possibles_abr", texte: "Valeurs possibles dans un arbre binaire de recherche"}
]
)