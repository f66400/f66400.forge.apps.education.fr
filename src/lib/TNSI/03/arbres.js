export function creer_arbre_aleatoire(seuils=[0, 0.2, 0.5, 0.7]){ // seuils : prob d'être vide par niveau
    let tab = [];
    let alea = Math.random();
    if(alea < seuils[0]){ 
        tab.push({etiquette:"vide"})
    }else{
         tab.push({etiquette:" "})
    }
    for(let i=1; i<Math.pow(2,seuils.length)-1; i++){
        alea = Math.random();
		if(i==2 && tab[1].etiquette === "vide"){  // éviter l'arbre-racine
			tab.push({etiquette:" "});
	  }
    else if( alea < seuils[ Math.floor(Math.log2(i+1)) ] || tab[ Math.floor((i-1)/2) ].etiquette === "vide" ){
             tab.push({etiquette:"vide"})
        }else{
             tab.push({etiquette:" "})
        }
    }
    return tab;
}
export function est_feuille(index, liste){
    if( index*2+1 >= liste.length){
        return true;
    }else{
        return liste[index*2+1].etiquette =="vide" && liste[index*2+2].etiquette =="vide";
    }
}
export function taille(liste){
    return liste.filter((x) => x.etiquette !== "vide").length;
}
export function hauteur(liste, debut=0){
    if(debut >= liste.length || liste[debut].etiquette === "vide"){
        return 0;
    }
    else if(est_feuille(debut, liste)){
        return 1
    }else{
        return 1 + Math.max( hauteur(liste, 2*debut+1), hauteur(liste,2*debut+2) )
    }
}
export function parcours_largeur(liste){
	let tab = [];
	for(let i=0; i<liste.length; i++){
		if(liste[i].etiquette !== "vide"){
			tab.push(i)
		}
	}
	return tab;
}

export function parcours_prefixe(liste){
	let tab = [];
	function parcours(i){
		if(i >= liste.length || liste[i].etiquette == "vide"){
			return 0;
		}
		else{
			tab.push(i);
			parcours(2*i+1);
			parcours(2*i+2);
		}
	}
	parcours(0);
	return tab;
}

export function parcours_infixe(liste){
	let tab = [];
	function parcours(i){
		if(i >= liste.length || liste[i].etiquette == "vide"){
			return 0;
		}
		else{
            parcours(2*i+1);
			tab.push(i);
			parcours(2*i+2);
		}
	}
	parcours(0);
	return tab;
}
export function parcours_suffixe(liste){
	let tab = [];
	function parcours(i){
		if(i >= liste.length || liste[i].etiquette == "vide"){
			return 0;
		}
		else{
			parcours(2*i+1);
			parcours(2*i+2);
            tab.push(i);
		}
	}
	parcours(0);
	return tab;
}
export function inserer_valeur_abr(liste, valeur){
	let pos = 0;
	while(liste[pos].etiquette !== " "){
		if(valeur < parseInt(liste[pos].etiquette)){
			pos = 2*pos+1
		}else{
			pos = 2*pos+2
		}
	}
	return pos;
}