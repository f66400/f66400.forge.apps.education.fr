import { readable } from 'svelte/store';

export const contenus = readable(
[{
    href: "/TNSI/01/01-observer_reconnaitre",
    texte: "Observer, reconnaitre"
  },
  {
    href: "/TNSI/01/02-retourner_pile",
    texte: "Retourner une pile"
  },
  {
    href: "/TNSI/01/03-retourner_pile_python",
    texte: "Retourner une pile avec Python"
  },
  {
    href: "/TNSI/01/04-princesse_petit_pois",
    texte: "La Princesse au petit pois"
  },
  {
    href: "/TNSI/01/05-trois_lits_princesse",
    texte: "Les trois lits de la princesse"
  },
  {
    href: "/TNSI/01/06-princesse_petit_pois_python",
    texte: "La Princesse et le Python"
  },
  {
    href: "/TNSI/01/07-trois_lits_princesse_python",
    texte: "Les trois lits, la princesse et le Python"
  },
  {
    href: "/TNSI/01/08-file_deux_piles_enfiler",
    texte: "Une file avec deux piles : enfiler"
  },
  {
    href: "/TNSI/01/09-file_deux_piles_enfiler_defiler",
    texte: "Une file avec deux piles : enfiler et défiler"
  },
  {
    href : "/TNSI/01/10-file_deux_piles_aleatoire",
    texte: "Une file avec deux piles : exercice aléatoire"
  },
  {
    href : "/TNSI/01/11-file_deux_piles_reconstituer_file",
    texte: "Une file avec deux piles : retrouver la file"
  },
  { href: "/TNSI/01/12-retourner_pile_tout_python",
    texte: "Retourner une pile, 100 % Python"
  }
  /*
  ,
  { href: "/TNSI/01/21-capytale-f6c2-626655",
    texte: "Parcours de files (sur Capytale)"
  },
  { href: "/TNSI/01/22-capytale-5d40-628758",
    texte: "Parcours de piles (sur Capytale)"
  }
  */
]
);