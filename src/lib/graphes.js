import * as d3 from 'd3';
import {est_symetrique, est_booleenne, creer_matrice} from '$lib/matrices.js';

export function dessiner_graphe(nodes, links, id, params={}){
    /*
    params.pondere: bool
    params.oriente: bool
    */
    console.log('dessiner graphe')

    let pondere = links[0]?.weight ? true: false // un seul poids indiqué => pondéré  //params?.pondere ? true : false;
    let oriente = params?.oriente ? true : false;

    console.log(pondere+" "+oriente);
    console.log(nodes.length+" "+links.length);

    d3.selectAll(`#${id} > *`).remove(); // effacer d'abord


    // Créer l'élément SVG
    let link = d3.select(`#${id}`)
        .selectAll('.link')
        .data(links)
        .enter().append('path')
        .attr('class', 'link');
    let weight;  // let dans un if est une variable locale du bloc
    if (pondere){
        weight = d3.select(`#${id}`).append("g")
        .selectAll(".weight")
        .data(links)
        .enter().append("text")
        .attr("class", "weight")
        .text(d => d.weight)
        .attr("text-anchor", "middle")
        .attr('fill', '#999');
    }
   

    let node = d3.select(`#${id}`)
        .selectAll('.node')
        .data(nodes)
        .enter().append("g")
        .attr('class', 'node')
        .style('cursor', 'pointer');

    let circles = node.append("circle")
        .attr('r', 20)
        .attr('fill', '#999')
        .attr('stroke', 'var(--monbleu)')
        .attr('stroke-width', 5);

    let labels = node.append("text")
        .text(function(d){ return(d.id)})
        .style('text-anchor', 'middle')
        .attr('fill', 'white')
        .attr('y', 5);

  // Initialiser la simulation
    let simulation = d3.forceSimulation(nodes)
        .force('link', d3.forceLink(links).id(d => d.id).distance(100))
        .force('charge', d3.forceManyBody().strength(-200))          // Répulsion entre les nœuds
        .force('center', d3.forceCenter(200, 200))                   // Centrer le graphe


    const encadrer = function(x){ // pour ne pas sortir du cadre
        if(x < 10){
            return 10
        }else if(x > 380){
            return 380
        }else{
            return x
        }
    }

    function poids(d){
        var offset = 15;

        var midpoint_x = (d.source.x + d.target.x) / 2;
        var midpoint_y = (d.source.y + d.target.y) / 2;

        var dx = (d.target.x - d.source.x);
        var dy = (d.target.y - d.source.y);

        var normalise = Math.sqrt((dx * dx) + (dy * dy));

        var offSetX = midpoint_x + offset*(dy/normalise);
        var offSetY = midpoint_y - offset*(dx/normalise);

        return [offSetX, offSetY];

    }
    function arc(d) {
        var offset = 30;

        var midpoint_x = (d.source.x + d.target.x) / 2;
        var midpoint_y = (d.source.y + d.target.y) / 2;

        var dx = (d.target.x - d.source.x);
        var dy = (d.target.y - d.source.y);

        var normalise = Math.sqrt((dx * dx) + (dy * dy));

        var offSetX = midpoint_x + offset*(dy/normalise);
        var offSetY = midpoint_y - offset*(dx/normalise);

        var xFleche =  midpoint_x + 0.5*offset*(dy/normalise);
        var yFleche =  midpoint_y - 0.5*offset*(dx/normalise);

        let coef = 10;

        return "M " + d.source.x + "," + d.source.y +
            " Q " + offSetX + "," + offSetY +
            " " + d.target.x + "," + d.target.y +
            "M " + xFleche + "," + yFleche + // bout de la flèche
            " L " + (xFleche - coef*dx/normalise - 0.5*coef*dy/normalise ) + "," + (yFleche - coef*dy/normalise + 0.5*coef*dx/normalise) +
            " " + (xFleche - coef*dx/normalise + 0.5*coef*dy/normalise) + "," + (yFleche - coef*dy/normalise - 0.5*coef*dx/normalise) +
            " " + xFleche + "," + yFleche
    }

    function segment(d){
        return  "M " + d.source.x + "," + d.source.y +" L "+ d.target.x + "," + d.target.y;
    }

  // Mise à jour des positions des nœuds et des arêtes à chaque tick de la simulation
  simulation.on('tick', () => {
    if(oriente){
        link.attr('d', d => arc(d))
    }else{
        link.attr('d', d => segment(d))
    }
    link
    .style('stroke', '#999')
    .style('stroke-width', 2)
    .style('fill', 'none')
    
    node
      .attr("transform", function(d) {
        return "translate(" + encadrer(d.x) + "," + encadrer(d.y) + ")";
      })
    if(pondere){
    weight
    .attr("x", d => poids(d)[0]) //d => (d.source.x + d.target.x) / 2)
    .attr("y", d => poids(d)[1]) //d => (d.source.y + d.target.y) / 2)
    }
   
  });

  // partie drag
    node.call(d3.drag()
        .on("start", dragstarted)
        .on("drag", dragged)
        .on("end", dragended));
    
    // Reheat the simulation when drag starts, and fix the subject position.
    function dragstarted(event) {
        if (!event.active) simulation.alphaTarget(0.3).restart();
        event.subject.fx = event.subject.x;
        event.subject.fy = event.subject.y;
    }

    // Update the subject (dragged node) position during drag.
    function dragged(event) {
        event.subject.fx = encadrer(event.x);
        event.subject.fy = encadrer(event.y);
    }

    // Restore the target alpha so the simulation cools after dragging ends.
    // Unfix the subject position now that it’s no longer being dragged.
    function dragended(event) {
        if (!event.active) simulation.alphaTarget(0);
        event.subject.fx = null;
        event.subject.fy = null;
    }
}

export function matrice_en_d3(matrice){  // matrice en [noeuds, aretes] pour d3
    const oriente = ! est_symetrique(matrice);
    const pondere = ! est_booleenne(matrice);
	const ordre = matrice.length;
	let noeuds = [];
	for(let i = 0; i<ordre; i++){
		noeuds.push(
			{
				id: i,
				label: i.toString()
			}
		)
	}
	let aretes = [];
	for(let i = 0; i<ordre; i++){
		for(let j = 0; j<i; j++){
			if( matrice[i][j]!=0 ){
                if(pondere){
                    aretes.push( { source: i, target: j, weight: matrice[i][j] } )
                }else{
                    aretes.push( { source: i, target: j } )   
                }
			}
            if( oriente && matrice[j][i]!=0 ){  // graphe orienté
				if(pondere){
                    aretes.push( { source: j, target: i, weight: matrice[j][i] } )
                }else{
                    aretes.push( { source: j, target: i } )   
                }
			}
		}
	}
    console.log("aretes : "+JSON.stringify(aretes));
	return [noeuds, aretes];
}

export function liste_adjacence_en_matrice(tab){
    // on construit une matrice
    let n = tab.length;
    let matrice = creer_matrice(n,n);
    let m;
    for(let i = 0; i<n; i++){
       m = tab[i].length;
       for(let j=0; j<m; j++){
            matrice[i][ tab[i][j] ] = 1
       }
    }
    return matrice
}
export function liste_adjacence_en_d3(tab){
    return matrice_en_d3( liste_adjacence_en_matrice(tab) )
}