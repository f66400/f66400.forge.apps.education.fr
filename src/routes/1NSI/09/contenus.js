import { readable } from 'svelte/store';

export const contenus = readable(
    [{
        href: "/1NSI/09/01-definition",
        texte: "Définition"
    },
    {
        href: "/1NSI/09/02-lecture",
        texte: "Lecture d'un élément"
    },
    {
        href: "/1NSI/09/03-longueur",
        texte: "Longueur d'un tableau"
    },
    {
        href: "/1NSI/09/04-modification",
        texte: "Modification d'un élément"
    },
    {
        href: "/1NSI/09/05-ajout",
        texte: "Ajout d'un élément"
    },
    {
        href: "/1NSI/09/06-suppression",
        texte: "Suppression d'un élément"
    },
    {
        href: "/1NSI/09/07-matrices",
        texte: "Matrices"
    },
    {
        href: "/1NSI/09/08-matrices-element-python",
        texte: "Matrices : lecture ou modification un élément"
    }
]
);