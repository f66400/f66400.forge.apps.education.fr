import { readable } from 'svelte/store';

export const contenus = readable(
    [{
        href: '/1NSI/03/01-et', texte: 'ET'
    },
    {
        href: '/1NSI/03/02-ou', texte: 'OU'
    },
    {
        href: '/1NSI/03/03-ou_exclusif', texte: 'OU exclusif'
    },
    {
        href: '/1NSI/03/04-non_et', texte: 'NON-ET'
    },
    {
        href: '/1NSI/03/05-non_ou', texte: 'NON-OU'
    },
    {
        href: '/1NSI/03/06-et_non', texte: 'ET NON'
    },
    {
        href: '/1NSI/03/07-ou_non', texte: 'OU NON'
    },
    {
        href: '/1NSI/03/08-quel_circuit', texte: 'Quel circuit ?'
    }
    ]
);