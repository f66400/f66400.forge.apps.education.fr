import { readable } from 'svelte/store';

export const contenus = readable(
    [{
        href: "/1NSI/01/01-quatre_taches_ordinateur",
        texte: "Les quatre tâches de l'ordinateur"
    },
    {
        href: "/1NSI/01/02-binaire",
        texte: "Le binaire"
    },
    {
        href: "/1NSI/01/03-performance_ordinateur",
        texte: "Performances d'un ordinateur"
    },
    {
        href: "/1NSI/01/04-systemes-exploitation",
        texte: "Systèmes d'exploitation"
    }]
);