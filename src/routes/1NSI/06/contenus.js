import { readable } from 'svelte/store';

export const contenus = readable(
    [
    {
        href: "/1NSI/06/01-capytale-6946-2129856",
        texte: "Créer une fonction"
    },
    {
        href: "/1NSI/06/02-capytale-3b89-902250",
        texte: "Le mot-clé return"
    },
    {
        href: "/1NSI/06/03-capytale-2f0f-2167295",
        texte: "Documenter une fonction"
    },
    {
        href: "/1NSI/06/04-capytale-10f0-2195014",
        texte: "Les assertions"
    }
    ]
);