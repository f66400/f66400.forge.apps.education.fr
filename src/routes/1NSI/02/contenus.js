import { readable } from 'svelte/store';

export const contenus = readable(
    [
    {
        href: "/1NSI/02/01-connexion-capytale",
        texte: "Connexion à Capytale"
    },
    {
    href: "/1NSI/02/02-capytale-9c67-3755004",
    texte: "Premiers programmes (sur Capytale)"
    },
    {
        href: "/1NSI/02/03-capytale-6161-3810912",
        texte: "Variables, affectation, commentaires et opérations arithmétiques"
    }
    ]
);